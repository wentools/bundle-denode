// deno-lint-ignore-file camelcase
import { createNpmPackage } from './create-npm-package.ts'
import { fetchProjectInfo } from './fetch-project-info.ts'
import { join } from '../deps.ts'
import { writeFile } from '../write-file.ts'
import { parseVersion } from './parse-version.ts'
import { parseProjectName } from './parse-project-name.ts'

const packageFileInputPath = 'package.json'
const packageFileOutputDirPath = 'dist/node'

interface PackageOptions {
  main: string
  module: string
  typings: string
  files: string[]
}

const createPackage = async ({
  main,
  module,
  typings,
  files,
}: PackageOptions) => {
  const { description, tag_list } = await fetchProjectInfo()

  const npmPackage = createNpmPackage({
    ...JSON.parse(await Deno.readTextFile(packageFileInputPath)),
    // name: parseProjectName(Deno.env.get('CI_PROJECT_PATH')),
    description,
    version: parseVersion(Deno.env.get('CI_COMMIT_TAG')),
    repository: {
      type: 'git',
      url: `git+${Deno.env.get('CI_PROJECT_URL')}.git`,
    },
    homepage: `${Deno.env.get('CI_PROJECT_URL')}#readme`,
    keywords: tag_list,
    main,
    module,
    typings,
    files,
    sideEffects: false,
  })

  await writeFile(
    JSON.stringify(npmPackage, null, 2),
    join(packageFileOutputDirPath, packageFileInputPath)
  )
}

export { createPackage }
