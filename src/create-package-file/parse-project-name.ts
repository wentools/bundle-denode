const parseProjectName = (text = '') => {
  return text.includes('/') ? `@${text}` : text
}

export { parseProjectName }
