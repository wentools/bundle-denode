interface NpmPackage {
  name: string
  version: string
  description: string
  main: string
  typings: string
  files: string[]
  sideEffects: boolean
  module: string
  repository: {
    type: string
    url: string
  }
  keywords: string[]
  author: string
  license: string
  homepage: string
}

const createNpmPackage = (
  npmPackage: Partial<NpmPackage>
): NpmPackage => {
  const {
    author = '',
    description = '',
    files = [],
    homepage = '',
    keywords = [],
    module = '',
    license = '',
    main = '',
    name = '',
    repository,
    sideEffects = false,
    typings = '',
    version = '',
  } = npmPackage

  const { type = '', url = '' } = repository ?? {}

  return {
    author,
    description,
    files,
    homepage,
    keywords,
    license,
    main,
    name,
    module,
    repository: {
      type,
      url,
    },
    sideEffects,
    typings,
    version,
  }
}

export { createNpmPackage }
export type { NpmPackage }
