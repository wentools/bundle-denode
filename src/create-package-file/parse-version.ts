const parseVersion = (text = '') => {
  let versionText = text
  if (text.startsWith('v.') || text.startsWith('V.')) {
    versionText = versionText.substring(2)
  }
  if (text.startsWith('v') || text.startsWith('V')) {
    versionText = versionText.substring(1)
  }

  return versionText
}

export { parseVersion }
