// deno-lint-ignore-file camelcase
interface MaybeProjectInfo {
  tag_list?: string[]
  description?: string
}

interface ProjectInfo {
  tag_list: string[]
  description: string
}

const createProjectInfo = (info: MaybeProjectInfo): ProjectInfo => {
  const { description = '', tag_list = [] } = info

  return {
    description,
    tag_list,
  }
}

const fetchProjectInfo = async () => {
  const resp = await fetch(
    `${Deno.env.get('CI_API_V4_URL')}/projects/${Deno.env.get(
      'CI_PROJECT_ID'
    )}`
  )
  const data = await resp.json()

  return createProjectInfo(data)
}

export { fetchProjectInfo }
export type { ProjectInfo }
