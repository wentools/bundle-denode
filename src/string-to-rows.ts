const stringToRows = (text: string) => {
  return text.split(/\r?\n/)
}

export { stringToRows }
