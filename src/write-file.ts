import { dirname, exists } from './deps.ts'

const writeFile = async (text: string, path: string) => {
  const dir = dirname(path)

  if (!(await exists(dir))) {
    await Deno.mkdir(dir, { recursive: true })
  }

  await Deno.writeTextFile(path, text)
}

export { writeFile }
