import { writeFile } from '../../write-file.ts'
import { createFileOutputPath } from '../create-file-output-path.ts'
import { parseFileText } from './parse-file-text.ts'

const bundleNodeFile = async (
  filePath: string,
  outputPath: string
) => {
  const fileContent = await Deno.readTextFile(filePath)
  const newText = parseFileText(fileContent)
  const nodeNewPath = createFileOutputPath(outputPath, filePath)

  writeFile(newText, nodeNewPath), console.log('wrote file:')
  console.log(nodeNewPath)
}

export { bundleNodeFile }
