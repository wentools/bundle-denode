import { stringToRows } from '../../string-to-rows.ts'

const parseRow = (text: string) => {
  let newText = text
  if (
    (text.includes('import') || text.includes('export')) &&
    text.includes(".ts'")
  ) {
    newText = text.replace('.ts', '')
  }
  return newText
}

const parseFileText = (text: string) => {
  const rows = stringToRows(text)

  return rows.map(parseRow).join('\n')
}

export { parseFileText }
