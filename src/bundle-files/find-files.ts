const findFiles = async (entry = 'src/mod.ts') => {
  const { files } = await Deno.emit(entry)

  const filesList: string[] = []

  for (const [fileName, text] of Object.entries(files)) {
    if (fileName.endsWith('ts.js.map')) {
      filesList.push(
        JSON.parse(text).sources[0].replace('file://', '')
      )
    }
  }

  return filesList
}

export { findFiles }
