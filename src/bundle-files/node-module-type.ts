type NodeModuleType = 'commonjs' | 'es6'

export type { NodeModuleType }
