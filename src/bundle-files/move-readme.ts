import { join, exists, copy } from '../deps.ts'

const moveReadme = async (outputPath: string) => {
  const file = (await exists('readme.md'))
    ? 'readme.md'
    : (await exists('README.md'))
    ? 'README.md'
    : ''

  if (file.length > 0) {
    try {
      const output = join(outputPath, 'node', file)
      await copy(file, output)
      console.log(`Copied readme to: ${output}`)
    } catch {
      console.error('Was unable to copy readme to node bundle')
    }
  }
}

export { moveReadme }
