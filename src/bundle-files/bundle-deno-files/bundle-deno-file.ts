import { writeFile } from '../../write-file.ts'
import { createFileOutputPath } from '../create-file-output-path.ts'

const bundleDenoFile = async (
  filePath: string,
  outputPath: string
) => {
  const fileContent = await Deno.readTextFile(filePath)
  const denoNewPath = createFileOutputPath(outputPath, filePath)

  writeFile(fileContent, denoNewPath), console.log('wrote file:')
  console.log(denoNewPath)
}

export { bundleDenoFile }
