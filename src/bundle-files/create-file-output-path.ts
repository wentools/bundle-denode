import { dirname, join, relative, basename } from '../deps.ts'

const createFileOutputPath = (
  outputPath: string,
  filePath: string
) => {
  const dir = dirname(filePath)
  return join(dir, relative(dir, outputPath), basename(filePath))
}

export { createFileOutputPath }
