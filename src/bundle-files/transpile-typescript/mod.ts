import type { NodeModuleType } from '../node-module-type.ts'
import type { TranspileTarget } from '../transpile-target.ts'

interface TranspileOptions {
  transpileTarget?: TranspileTarget
  module?: NodeModuleType
}

const transpileTypescript = async (
  outputPath: string,
  startFile: string,
  options: TranspileOptions = {}
) => {
  const { transpileTarget = 'ES6', module = 'es6' } = options

  console.log('outputPath:', outputPath)

  console.log('startFile', startFile)
  console.log('transpileTarget', transpileTarget)

  const cmd = Deno.run({
    cmd: [
      'tsc',
      startFile,
      '--declaration',
      '--module',
      module,
      '--target',
      transpileTarget,
    ],
    stdout: 'piped',
    stderr: 'piped',
  })

  const { code } = await cmd.status()

  if (code !== 0) {
    console.error('Was unable to transpile')
  }

  const rawOutput = await cmd.output()
  await Deno.stdout.write(rawOutput)

  cmd.close()
}

export { transpileTypescript }
