type TranspileTarget =
  | `ES3`
  | `ES5`
  | `ES6`
  | `ES2015`
  | `ES7`
  | `ES2016`
  | `ES2017`
  | `ES2018`
  | `ES2019`
  | `ES2020`
  | `ESNext`

export type { TranspileTarget }
