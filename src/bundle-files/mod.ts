import type { NodeModuleType } from './node-module-type.ts'
import type { TranspileTarget } from './transpile-target.ts'
import { basename, join } from '../deps.ts'
import { findFiles } from './find-files.ts'
import { bundleDenoFile } from './bundle-deno-files/mod.ts'
import { bundleNodeFile } from './bundle-node-file/mod.ts'
import { moveReadme } from './move-readme.ts'
import { transpileTypescript } from './transpile-typescript/mod.ts'

interface BundleOptions {
  runtime: 'deno' | 'node'
  nodeModuleDirName?: string
  nodeModule?: NodeModuleType
  transpileTarget?: TranspileTarget
}

const bundleFiles = async (
  outputPath: string,
  startFile: string,
  bundleOptions: BundleOptions[] = []
) => {
  const files = await findFiles(startFile)

  console.log('Files found by Deno.emit:')
  console.log(files)

  await Promise.all(
    files.map((filePath) =>
      Promise.all(
        bundleOptions.map((bundleOption) =>
          bundleOption.runtime === 'deno'
            ? bundleDenoFile(filePath, join(outputPath, 'deno'))
            : bundleNodeFile(
                filePath,
                join(
                  outputPath,
                  'node',
                  bundleOption.nodeModuleDirName ?? ''
                )
              )
        )
      )
    )
  )

  await Promise.all(
    bundleOptions
      .filter(({ runtime }) => runtime === 'node')
      .map(
        ({
          nodeModule: module,
          nodeModuleDirName,
          transpileTarget,
        }) =>
          transpileTypescript(
            join(outputPath, 'node', nodeModuleDirName ?? ''),
            join(
              outputPath,
              'node',
              nodeModuleDirName ?? '',
              basename(startFile)
            ),
            { module, transpileTarget }
          )
      )
  )

  await moveReadme(outputPath)
}

export { bundleFiles }
