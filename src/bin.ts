import { bundleFiles } from './bundle-files/mod.ts'
import { createPackage } from './create-package-file/mod.ts'
import { basename, parseFlags, OptionType, join } from './deps.ts'

const result = parseFlags(Deno.args, {
  flags: [
    {
      name: 'source',
      aliases: ['s'],
      type: OptionType.STRING,
    },
    {
      name: 'out-dir',
      aliases: ['o'],
      type: OptionType.STRING,
    },
  ],
})

const startFile: string = result.flags['source'] ?? 'src/mod.ts'
const outputPath: string = result.flags['out-dir'] ?? 'dist'

const transpileTarget = 'ES6'

await bundleFiles(outputPath, startFile, [
  {
    runtime: 'deno',
  },
  {
    runtime: 'node',
    nodeModuleDirName: 'cjs',
    nodeModule: 'commonjs',
    transpileTarget,
  },
  {
    runtime: 'node',
    nodeModuleDirName: 'esm',
    nodeModule: 'es6',
    transpileTarget,
  },
])

const baseStartFile = basename(startFile)

await createPackage({
  main: join('cjs', baseStartFile.replace('.ts', '.js')),
  module: join('esm', baseStartFile.replace('.ts', '.js')),
  typings: join('cjs', baseStartFile.replace('.ts', '.d.ts')),
  files: ['**/*.js', '**/*.d.ts'],
})
